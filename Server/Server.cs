﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Collections;
using System.Runtime.Remoting;

namespace Projecto_PadInt {
    public class Server : MarshalByRefObject, IServer {

        private Dictionary<int, PadIntSS> padints = new Dictionary<int, PadIntSS>();
        private Dictionary<int, PadIntSS> replicas = new Dictionary<int, PadIntSS>();
        private TransactionManager _transactionManager = new TransactionManager();
        private  string myAddr;
        private string backUpServer;
        private  IMaster master;
        private string MASTERADDRESS;

        public Server(string address) {
            padints = new Dictionary<int, PadIntSS>();
            _transactionManager = new TransactionManager();
            MASTERADDRESS = "tcp://localhost:8086/Master";
            myAddr = address + "/Server";
            padints.Add(345678, new PadIntSS(345678));
            }

        public void init() {
                try {
            master = (IMaster)Activator.GetObject(typeof(IMaster), MASTERADDRESS);
            System.Console.WriteLine(master.regServer(myAddr));
            Console.WriteLine("Server Started ");
            }
        catch (SocketException) {
            System.Console.WriteLine("Could not locate Master server");
            }
        }

        public TransactionManager transactionManager {
            get { return _transactionManager; }
            set { _transactionManager = value; }
            }

        public void deletePadInt(int id){
            padints.Remove(id);
            }


        public void replicate(string address) {
        System.Console.WriteLine("VOU AO servidor com o endereço:" + address);
            List<int> lista;
            try {
                Server servidor = (Server)Activator.GetObject(typeof(Server), address);
                servidor.backupInfo(myAddr);
                lista = servidor.backup();
                for(int i=0; i<lista.Count;++i) {
                    replicas.Add(lista.ElementAt(i), new PadIntSS(lista.ElementAt(i), lista.ElementAt(++i)));
                    }
                
                }
            catch (SocketException) {
                System.Console.WriteLine("Could not locate server to replicate");
                }
            catch (RemotingException) {
                System.Console.WriteLine("ERROR - There was a problem with the server to replicate");
                }
            System.Console.WriteLine("Fui buscar os objectos do servidor com o endereço:" + address);
            }

        public List<int> backup() {
        System.Console.WriteLine("vou mandar os meus padints");
            List<int> lista= new List<int>();
            foreach (PadIntSS p in padints.Values){
                lista.Add(p.id);
                lista.Add(p.valor);
                }
            return lista;
            }
        public void backupInfo(string serverAddr) {
            backUpServer = serverAddr;
            }

        public void actualizaReplica(PadIntSS padint){
            if(replicas.ContainsKey(padint.id))
            replicas[padint.id] = padint;
        }

        public void sendUpdate() {
        foreach (Transaction t in transactionManager.activeTransactions.Values) {
            foreach (PadIntSS p in t.padInts.Values) {
                try {
                    Server servidor = (Server)Activator.GetObject(typeof(Server), backUpServer);
                    servidor.actualizaReplica(p);
                    }
                catch (RemotingException) {
                    System.Console.WriteLine("ERROR - There was a problem with the server to replicate");
                    }
                }
            }
          }
        public PadIntSS getPadInt(int id, int clientId, bool create) {

            // se nao estiver pergunta ao master onde deve estar
            string servAddr = master.getServerByPadInt(id);
            Console.WriteLine("Getting PadInt id: " + id + " create?-" + create);

            if (transactionManager.activeTransactions.ContainsKey(clientId))
                transactionManager.getTransaction(clientId).addParticipant(servAddr);
            else { Console.WriteLine("[SERVER] - PadInt ou Transacção não encontrada"); }

            if (servAddr == myAddr) {
                return getPadIntFromSelf(id, create, clientId);
                }
            else {
                return getPadIntFromServer(id, servAddr, create, clientId);
                }
        }

        public PadIntSS getPadIntFromServer(int id, string addr, bool create, int clientId){
            Server servidor = (Server)Activator.GetObject(typeof(Server), addr);
            Console.WriteLine("Requested Padint com id: " + id);
            
            //TRY CATCH REMOTING EXCEPTION
            return servidor.getPadIntFromSelf(id, create, clientId);            
        }

        public PadIntSS getPadIntFromSelf(int id, bool create, int clientId) {
            PadIntSS aux;
            try{
                if (create)
                {
                    aux = new PadIntSS(id,this);
                    padints.Add(id,aux);
                    Console.WriteLine("HERE! Padint com id: " + id);
                }
                else {
                    Console.WriteLine("HERE! Creating Padint com id: " + id);
                    aux = padints[id];
                }

                if (transactionManager.activeTransactions.ContainsKey(clientId))
                    transactionManager.getTransaction(clientId).addPadint(aux);
                else { transactionManager.newTransaction(clientId).addPadint(aux); }
                    return aux;
            }
            catch (KeyNotFoundException){
                Console.WriteLine("[SERVER] - PadInt ou Transacção não encontrada");
                return null;
            }
        }

        public bool Fail(){
        Console.WriteLine("FAIL Received!");
            RemotingServices.Disconnect(this);
            return true;
            }

        public bool Recover() {
            /*RemotingServices.Marshal(this, "Server", typeof(Server));
            Console.WriteLine("mandarem-me acima!!");*/
            return true;
            }

        static void Main(string[] args) {
            // Getting Port

            Server myServer;
            System.Console.WriteLine("Insira o Porto:");
            int porto = Int32.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Insira o Endereço: [ou ENTER para default: tcp://localhost:"+ porto.ToString() +"/Server]");
            string address=System.Console.ReadLine();

            TcpChannel canal1 = new TcpChannel(porto);
            ChannelServices.RegisterChannel(canal1, false);

            if (address == "")
                myServer = new Server("tcp://localhost:" + porto.ToString());
            else { myServer = new Server(address); }

            myServer.init();
            RemotingServices.Marshal(myServer, "Server", typeof(Server));
            System.Console.WriteLine("Deseja replicar algum servidor? S ou N");

            if (System.Console.ReadLine()=="s"){
            System.Console.WriteLine("Insira o Endereço: [ou ENTER para default: tcp://localhost: PORTO /Server]");
            string originalAddress = System.Console.ReadLine();
            if (originalAddress == "") {
                 System.Console.WriteLine("Insira o porto do servidor a replicar:"); ;
            string originalServerPort = System.Console.ReadLine();
            originalAddress = "tcp://localhost:" + originalServerPort + "/Server";
                }

            myServer.replicate(originalAddress);
            }

            

            System.Console.ReadLine();


        }
    }
}