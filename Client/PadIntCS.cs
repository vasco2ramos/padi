﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projecto_PadInt;
using System.Threading;

namespace PADI_DSTM {
    public class PadIntCS : PadInt {

        int _id;

        public PadIntCS(int id){
            _id = id;
            }

        public int Read() {
            return PadiDstm.Read(_id);
            }

        public void Write(int valor){
            PadiDstm.Write(valor, _id);
            }
        }
    }
