﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Projecto_PadInt;
using System.Threading;

namespace PADI_DSTM {
    static public class PadiDstm {

        private static string masterAdress = "tcp://localhost:8086/Master";
        private static string serverAdress = "NAO INICIALIZADO";
        private static int clientId;
        private static Server servidor;
        private static IMaster master;
        private static Dictionary<int, PadIntSS> _buffer = new Dictionary<int, PadIntSS>();
        private static bool committed=true;
        

        public static bool Init() {
            master = (IMaster)Activator.GetObject(typeof(IMaster), masterAdress);
            serverAdress = master.getServer();
            clientId = master.getClientId();
            servidor = (Server)Activator.GetObject(typeof(Server), serverAdress); 
            return true;
            }

        public static bool TxBegin() {
        try {
            servidor.transactionManager.newTransaction(clientId);
            }
        catch (SocketException) {
            Console.WriteLine("Houve um problema ao começar a transacção no servidor com o endereço:" + serverAdress);
            Console.WriteLine("Vamos tentar na réplica");
            serverAdress = master.getServer();

            }
        committed = false;
            return true;
        }

        public static bool TxCommit() {
        if (servidor.transactionManager.sendPrepare(clientId)) {
            servidor.transactionManager.sendCommit(clientId);
            }
        //servidor.sendUpdate();
        servidor.transactionManager.clearTransaction(clientId);
        _buffer.Clear();
        committed = true;
            return true;
            }
        public static bool TxAbort() {
                   _buffer.Clear();
            return servidor.transactionManager.sendAbort(clientId);
            }
        public static bool Status() {
            Console.WriteLine("********************************************************");
            Console.WriteLine("Transacção com o id:" + clientId);
            Console.WriteLine("Cliente ligado ao servidor:" + serverAdress);

            Console.WriteLine("Acedeu aos padints:");
            foreach(PadIntSS p in _buffer.Values)
                Console.WriteLine("id:" + p.id + " valor:" + p.valor);
            Console.WriteLine("********************************************************");
        return true;
            }
        public static bool Fail(string URL) {
            Server servidor = (Server)Activator.GetObject(typeof(Server), URL);
            return servidor.Fail();
            }
        public static bool Freeze(string URL) { return true; }
        public static bool Recover(string URL) {
            //Server servidor = (Server)Activator.GetObject(typeof(Server), URL);
            //return servidor.Fail();
        return true;
            }

        public static PadInt CreatePadInt(int uid) {
            try {
                if (!_buffer.ContainsKey(uid))
            _buffer.Add(uid, servidor.getPadInt(uid, clientId, true));
                else { servidor.getPadInt(uid, clientId, true); }
                //servidor.transactionManager.addObject(clientId, temp);
            }
            catch (SocketException) {
                Console.WriteLine("Houve um problema ao criar PadInt com o id:" + uid);
                return null;
            }
            return new PadIntCS(uid);
        }

        public static PadInt AccessPadInt(int uid) {
        try {
        if (!_buffer.ContainsKey(uid))
            _buffer.Add(uid, servidor.getPadInt(uid, clientId, false));
        else { servidor.getPadInt(uid, clientId, false); }
            //servidor.transactionManager.addObject(clientId, temp);
            }
        catch (SocketException) {
            Console.WriteLine("Houve um problema ao aceder PadInt com o id:" + uid);
            return null;
            }
        return new PadIntCS(uid);
            }

        public static int Read(int id) {
            int result = _buffer[id].Read(clientId);
            if (result == -20123)
                    TxAbort();
                return result;
            }

        public static void Write(int valor, int id) {
        if (!(_buffer[id].Write(valor, clientId)))
            TxAbort();
        }

        }
    }
