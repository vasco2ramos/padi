﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projecto_PadInt;
using System.Threading;

namespace Projecto_PadInt
{
    public class CustomLock : MarshalByRefObject
    {

        private List<int> reading= new List<int>();
        private int TIMEOUT = 4000;
        private int writing = -1;
        // Lock to access reading list;
        Object lockR = new Object();
        // Lock to Wait for any Writers
        Object writesMonitor = new Object();

        private static int readerTimeouts = 0;
        private static int writerTimeouts = 0;
        private static int reads = 0;
        private static int writes = 0;
        

        public bool isReadLockHeld(int id)
        {
            return reading.Contains(id);
        }
        public bool isReadLockHeld()
        {
            return reading.Count != 0;
        }

        public bool isWriterLockHeld()
        {
            return writing != -1;
        }

        public bool isWriterLockHeld(int id)
        {
            return writing == id;
        }

        public bool acquireReaderLock(int id) {


        if (!(Monitor.TryEnter(writesMonitor, TIMEOUT)))
            return false;
        if (!(Monitor.TryEnter(lockR, TIMEOUT)))
            return false;
            if(!(reading.Contains(id)))
            reading.Add(id);
            Monitor.PulseAll(lockR);
            Monitor.Exit(lockR);
            Monitor.PulseAll(writesMonitor);
            Monitor.Exit(writesMonitor);

            return true;
        }

        public bool acquireWriterLock(int id) {
            bool lockTaken = false;
            Monitor.TryEnter(writesMonitor, TIMEOUT, ref lockTaken);
            writing = id;
            return lockTaken;

        }

        public bool upgradeReadLock(int id){
            if (!(Monitor.TryEnter(lockR, TIMEOUT)))
                return false;
            if (reading.Contains(id)) {
                reading.Remove(id);
                Monitor.PulseAll(lockR);
                Monitor.Exit(lockR);
                if (!(Monitor.TryEnter(writesMonitor, TIMEOUT)))
                    return false;
                writing = id;
                }
            else {
                Monitor.PulseAll(lockR);
                Monitor.Exit(lockR);
                }
            return true;
        }

        public void releaseWriterLock(){
        if (isWriterLockHeld()) {
            writing = -1;
            try {
                Monitor.PulseAll(writesMonitor);
                Monitor.Exit(writesMonitor);
                }
            catch (SynchronizationLockException) {
                Console.WriteLine("alguem sem lock ta a querer libertar");
                }
            }
       }

        public bool releaseReaderLock(int id)
        {

        if (!(Monitor.TryEnter(lockR, TIMEOUT)))
            return false;
            if (reading.Contains(id))
            {
                reading.Remove(id);                             
            }
            Monitor.PulseAll(lockR);
            Monitor.Exit(lockR);
            return true;
        }

        public void releaseLocks(int id) {
            if(id==writing)
                releaseWriterLock();
            releaseReaderLock(id);
        }
    }
}
