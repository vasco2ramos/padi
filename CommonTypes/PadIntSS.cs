﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projecto_PadInt;
using System.Threading;

namespace Projecto_PadInt {
     public class PadIntSS : MarshalByRefObject  {

        public static int TIMEOUT=5000;

        private int _id;
        private int _value;
        private int tryValue;
        private IServer _servidor;
        private CustomLock rwl = new CustomLock();

        private Object writeLock;
        
     

        public IServer servidor {
            get { return _servidor; }
            set { _servidor = value; }
            }
        public int valor {
            get { return _value; }
            set { _value = value; }
            }

        private List<IServer> transactionMembers; 
        
        public PadIntSS(int uid) {
            _value = -1;
            id = uid;
            }

        public PadIntSS(int uid, int valor) {
            _value = valor;
            id = uid;
            }

        public PadIntSS(int uid, IServer serv) {
            _value = -1;
            id = uid;
            _servidor = serv;
            }


        public int id {
            get { return _id; }
            set { _id = value; }
            }

        public int Read(int clientId) {
            if(rwl.acquireReaderLock(clientId))
                return _value;
            else {Console.WriteLine("FATAL ERROR - FAILED TO ACQUIRE READER LOCKS"); return -20123;}
        }

        public bool Write(int valor,int clientId) {
            bool go = true;
            if (rwl.isReadLockHeld(clientId))
            {
                go=rwl.upgradeReadLock(clientId);
            }
            else {
                go=rwl.acquireWriterLock(clientId);
            }
            if (go)
                tryValue = valor;
            else { Console.WriteLine("Failed to acquire locks -  ABORTING!");}
            return go; 
        }
        
        public void commit(int id) {
            _value = tryValue;
            rwl.releaseLocks(id);
            }

        public void abort(int id) {
            rwl.releaseLocks(id);
            }

        }
    }
