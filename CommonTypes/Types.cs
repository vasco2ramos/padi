﻿using System;
using System.Collections.Generic;

namespace Projecto_PadInt
{
    public interface IMaster
    {

       bool regServer(string serverAddress);

       String getServerByPadInt(int padIntId);
       string getServer();

       int getClientId();
       // String connect(int id, int porto);

        //String sendMessage(String msg, int sender);
    }

    public interface IServer {
        PadIntSS getPadInt(int id, int clientId, bool create);
        void init();
        PadIntSS getPadIntFromServer(int id, string addr, bool create, int clientId);
        PadIntSS getPadIntFromSelf(int id, bool create, int clientId);
        void deletePadInt(int id);
        TransactionManager transactionManager {get; set;}

        List<int> backup();
        void replicate(string address);
     }
 

    public interface PadInt
    {
        int Read();
        void Write(int valor);

    }
}
