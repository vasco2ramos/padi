﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projecto_PadInt {
    public class Transaction : MarshalByRefObject {

        private int txId; //O ID DA TRANSACÇÃO VAI SER O ID DO CLIENTE VISTO QUE CADA CLIENTE SO INICIA UMA TRANSACÇÃO DE CADA VEZ (É PRECISO REINICIAR INFO DA TX)

        private List<string> _participants = new List<string>();

        private Dictionary<int, PadIntSS> _padInts = new Dictionary<int, PadIntSS>();
        private Dictionary<int, PadIntSS> createdPadInts = new Dictionary<int, PadIntSS>();


        public void addParticipant(string p) {
            participants.Add(p);
            }

        public List<string> participants {
            get { return _participants; }
            set { _participants = value; }
             }

        public Dictionary<int, PadIntSS> padInts {
            get { return _padInts; }
            set { _padInts = value; }
            }

        public Transaction(int id) { 
            txId = id; 
            participants = new List<string>(); 
            _padInts = new Dictionary<int, PadIntSS>();
        }

        public bool doCommit() {
            foreach (PadIntSS aux in _padInts.Values) {
            aux.commit(txId);
                }
            createdPadInts.Clear();
            return true;
            }

        public bool doAbort() {
            foreach (PadIntSS aux in _padInts.Values) {
                aux.abort(txId);
                }
            foreach (PadIntSS aux in createdPadInts.Values) {
            Console.WriteLine("[ABORT]: Deleting Created PadInt with id:" + aux.id);
                aux.servidor.deletePadInt(aux.id);
                }
            return true;
            }

        public void addPadint(PadIntSS padint) {
            if (!_padInts.ContainsKey(padint.id)) {
                _padInts.Add(padint.id,padint);
                }
            }

        public void addCreatedPadint(PadIntSS padint) {
            if (!createdPadInts.ContainsKey(padint.id)) {
                createdPadInts.Add(padint.id, padint);
                }
            }

        public void clear(){
           // participants.Clear();
            _padInts.Clear();
            createdPadInts.Clear();
            }

        }
    }
