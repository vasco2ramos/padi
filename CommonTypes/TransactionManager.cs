﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projecto_PadInt;

namespace Projecto_PadInt {
    public class TransactionManager : MarshalByRefObject {

        private Dictionary<int, Transaction> _activeTransactions = new Dictionary<int, Transaction>(); //Transacçoes iniciadas pelo cliente especifico (uma de cada vez)
        //CADA cliente só pode ter iniciado uma transacção de cada vez


        public Dictionary<int, Transaction> activeTransactions {
            get { return _activeTransactions; }
            set { _activeTransactions = value; }
            }
        
        
        public Transaction newTransaction(int clientId) {
            Transaction temp = new Transaction(clientId);
            if (!(_activeTransactions.ContainsKey(clientId)))
                _activeTransactions.Add(clientId, temp);
            else { return _activeTransactions[clientId]; }
            return temp;
            }


        public void clearTransaction(int clientId){
            _activeTransactions.Remove(clientId);
            }

        public Transaction getTransaction(int clientId) {
            Transaction temp ;
            if ((temp = (_activeTransactions[clientId])) != null)
                return temp;
            else { Console.WriteLine("Não foi encontrada a transacção iniciada pelo cliente:" + clientId); return null; }
            }

        //METODO QUE SO O COORDENADOR EXECUTA
        public bool sendPrepare(int clientId){
            bool ready=true;
            IServer servidor;
            foreach(string participantAddr in getTransaction(clientId).participants){
                servidor = (IServer)Activator.GetObject(typeof(IServer), participantAddr);
                if (!(servidor.transactionManager.prepareToCommit(clientId)))
                    ready = false;
                } 
            return ready;
        }

        //METODO QUE SO O COORDENADOR EXECUTA
        public bool sendAbort(int clientId) {
            bool allAborted = true;
            IServer servidor;
            foreach (string participantAddr in getTransaction(clientId).participants) {
            servidor = (IServer)Activator.GetObject(typeof(IServer), participantAddr);
                if (!(servidor.transactionManager.doAbort(clientId)))
                    allAborted = false;
                } return allAborted;
            }

        //METODO QUE SO O COORDENADOR EXECUTA
        public bool sendCommit(int clientId) {
            bool commited = true;
            IServer servidor;
            foreach (string participantAddr in getTransaction(clientId).participants) {
            servidor = (IServer)Activator.GetObject(typeof(IServer), participantAddr);
                if (!(servidor.transactionManager.doCommit(clientId)))
                    commited = false;
                }
            return commited;
            }

        public bool prepareToCommit(int clientId){
            Console.WriteLine("VOU COMMITAR A TRANSACÇÃO DO CLIENTE:" + clientId);
            return true;
            }

        public bool doCommit(int clientId) {
            activeTransactions[clientId].doCommit();
            clearTransaction(clientId);
            return true;
        }

        public bool doAbort(int clientId) {
            activeTransactions[clientId].doAbort();
            clearTransaction(clientId);
            return true;
            }

        public void addObject(int clientId, PadIntSS padInt){
            if (_activeTransactions.ContainsKey(clientId))
                _activeTransactions[clientId].addPadint(padInt);
            }

        public void addCreated(int clientId, PadIntSS padInt) {
            if (_activeTransactions.ContainsKey(clientId))
                _activeTransactions[clientId].addPadint(padInt);
            }

        public void clear(int clientId){
            _activeTransactions.Remove(clientId);
            }
        public void clearAll(int clientId) {
            _activeTransactions.Clear();
            }

        }
    }
