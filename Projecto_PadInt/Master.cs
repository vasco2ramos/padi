﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;



namespace Projecto_PadInt
{
    class Master : MarshalByRefObject, IMaster {


        private Dictionary<int,string> _servidores = new Dictionary<int,string>();
        private int _clientId = 0;
        private Object clientIdLock = new Object();
        private int _serversNumber = 0;

        public bool regServer(String adress) {
            // Adding a server
            // Just a straight forward add for now, consider the server added asking for the objects.
            _servidores.Add(_serversNumber,adress);
            System.Console.WriteLine("Added Server with id: " + _serversNumber + " with adress: " + _servidores[_serversNumber]);
            _serversNumber++;
            return true;
        }

        public int getClientId() {
           //LOCK SOBRE CLIENT_ID
            int aux=_clientId;
            lock (clientIdLock) {
                aux = _clientId++;
                }
            return aux;
        }

        public String getServer(){
            int randomServer = new Random().Next(_servidores.Count);
            System.Console.WriteLine("Server request, returning server number: " + randomServer);
            return _servidores.Values.ElementAt(randomServer);
        }



        //retorna o numero do servidor (a começar em 0) responsavel pelo PadInt com o id especificado 
        public int checkRange(int id) {
            return id/(Int32.MaxValue / _serversNumber);
        }

        //retorna o endereço do servidor responsavel pelo PadInt com o id especificado
        public String getServerByPadInt(int padIntId) {
            return _servidores[checkRange(padIntId)];
           }


        static void Main(string[] args){
            TcpChannel channel = new TcpChannel(8086);
            ChannelServices.RegisterChannel(channel, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(Master),
                "Master",
                WellKnownObjectMode.Singleton);
            Console.WriteLine("A Servir........Enter para terminar.");
            Console.ReadLine();
        }
    }
}
